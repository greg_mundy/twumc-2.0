# Helper methods defined here can be accessed in any controller or view in the application

module Twumc
  class App
    module HistoryHelper
      # def simple_helper_method
      # ...
      # end
    end

    helpers HistoryHelper
  end
end
